# Sublime Jq

[jq](https://stedolan.github.io/jq/) wrapper for [Sublime Text](https://www.sublimetext.com/).

It gives you jq in the best of ways:

1. Format a JSON document
2. Interactively construct a jq query with live update
3. Run pre-defined jq queries on any documents

## Installation

### Via Package Control

Install it from [packagecontrol.io to benefit from automatic updates](https://packagecontrol.io/packages/Jq).

### Manual

Clone this repository in your Sublime Package directory (Find it in "Preferences"
and then "Browse Packages").

## Requirements

This plugin does not work on its own. It completely depends on jq.

Before using this plugin, you must have installed [jq](https://stedolan.github.io/jq/),
and you must ensure that `jq` is in your PATH.

You're done!

## How to use it

### Format JSON

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + P`, and search for `jq: Format JSON`.

Your tab content will be replaced with the formatted json.

![](screenshots/jq_pretty.gif)

### Format JSON: compact

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + P`, and search for `jq: Format JSON (compact)`.

Your tab content will be replaced with the formatted json as a 1-liner.

![](screenshots/jq_compact.gif)

### Interactively tranform JSON with a jq query

Within a given tab, start the Sublime Command Palette
with `Ctrl + Shift + P`, and search for `jq: Transform JSON`.

An input panel will be displayed at the bottom, and your JSON will be transformed
on the fly while you write your jq query.

If you aren't happy with your query, you can cancel it at anytime with `Escape`,
and the content of your tab is reverted to its previous content.

![](screenshots/jq_transform.gif)

## Commands Documentation

### `jq_format_json`

Given a tab, "pretty print" its json content. Equivalent of `jq '.'`.

Example:
```
view.run_command('jq_format_json')
```

### `jq_format_json_compact`

Given a tab, compress its json content on 1 line. Equivalent of `jq -c '.'`.

Example:
```
view.run_command('jq_format_json_compact')
```

### `jq_transform_json`

Given a tab, starts an interactive session to tranform its json content.
Equivalent to `jq 'query'`.

Example:
```
view.run_command('jq_transform_json')
```

### `jq_apply_query`

Given a tab, apply a pre-defined jq query to its content.

Example:
```
view.run_command('jq_apply_query', {'query': '.'})
```

Options:

- `query`: jq query as a string
- `compact`: `True` or `False` (default). Corresponds to `jq -c`
- `slurp`: `True` or `False` (default). Corresponds to `jq -s`.
