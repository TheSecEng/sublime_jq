import sublime
import sublime_plugin

import subprocess


def run_jq(text, query, line_endings, compact=False, slurp=False):
    options = []
    if compact:
        options.append('-c')
    if slurp:
        options.append('-s')

    command = ["jq"] + options + [query]

    startup_info = None

    if sublime.platform().lower() == 'windows':
        # Avoid showing console on windows (faster, and less flashy)
        startup_info = subprocess.STARTUPINFO()
        startup_info.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    with subprocess.Popen(command,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          startupinfo=startup_info) as proc:
        try:
            out, err = proc.communicate(input=bytes(text, 'utf-8'), timeout=10)
        except subprocess.TimeoutExpired:
            proc.kill()
            return 'jq process killed, because it took more than 10s'
        if proc.returncode > 0:
            output = err
        elif proc.returncode == 0:
            output = out
        else:
            return 'jq process killed'

        formatted_text = output.decode('utf-8')
        if line_endings.lower() == 'unix':
            formatted_text = formatted_text.replace('\r\n', '\n')
        return formatted_text if formatted_text[:4] != 'null' else text


class JqApplyQueryCommand(sublime_plugin.TextCommand):
    def run(self, edit, query, compact=False, slurp=False):
        size = self.view.size()
        region = sublime.Region(0, size)

        sublime_version = int(sublime.version())
        if 3000 <= sublime_version < 4000:
            self.view.set_syntax_file('Packages/JavaScript/JSON.sublime-syntax')
        if sublime_version >= 4000:
            self.view.assign_syntax('scope:source.json')

        text = self.view.substr(region)
        formatted_text = run_jq(text, query,
                                self.view.line_endings(), compact, slurp)
        self.view.replace(edit, region, formatted_text)

    def is_enabled(self):
        return True


class JqFormatJsonCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command('jq_apply_query', {'query': '.'})

    def is_enabled(self):
        return True


class JqFormatJsonCompactCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command('jq_apply_query', {'query': '.', 'compact': True})

    def is_enabled(self):
        return True


class JqSetTextCommand(sublime_plugin.TextCommand):
    def run(self, edit, text):
        self.view.set_read_only(False)
        size = self.view.size()
        region = sublime.Region(0, size)
        self.view.replace(edit, region, text)
        self.view.set_read_only(True)


class JqTransformJsonCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.set_read_only(True)
        size = self.view.size()
        region = sublime.Region(0, size)
        self.original_text = self.view.substr(region)
        self.view.window().show_input_panel('jq query', '.', self.on_done,
                                            self.on_change, self.on_cancel)

    def on_done(self, query):
        self.view.set_read_only(False)
        return

    def on_change(self, query):
        # Run jq in a short async request, to avoid slowing down user typing
        sublime.set_timeout_async(lambda: self.view.run_command('jq_set_text', {
            "text": run_jq(self.original_text, query,
                           self.view.line_endings())}), 1)

    def on_cancel(self):
        # Reset text to the original one
        self.view.run_command('jq_set_text', {"text": self.original_text})
        self.view.set_read_only(False)
